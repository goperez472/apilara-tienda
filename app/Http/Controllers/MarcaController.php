<?php

namespace App\Http\Controllers;

use App\Models\Marca;
use Illuminate\Http\Request;

class MarcaController extends Controller
{
    
    public function index()
    {
        $marcas = Marca::all();
        return response()->json($marcas);
    }

   
    public function store(Request $request)
{
    $rules = ['nombre' => 'required|string|min:1|max:100'];
    $validacion = \Validator::make($request->input(), $rules);

    if ($validacion->fails()) {
        return response()->json([
            'status' => false,
            'errors' => $validacion->errors()->all()
        ], 400);
    }

    $marca = new Marca($request->input());
    $marca->save();

    return response()->json([
        'status' => true,
        'message' => 'Marca registrada exitosamente'
    ], 201);
}


    public function show(Marca $marca)
    {
        return response()->json(['status' => true, 'data' => $marca]);
    }

    public function update(Request $request, Marca $marca)
    {
        $rules = ['nombre' => 'required|string|min:1|max:100'];
        $validacion = \Validator::make($request->input(),$rules);
        if($validacion->fails()){
         return response()->json([
             'status' => false,
             'errors' => $validacion->errors()->all()
         ],400);
        }
        $marca->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Marca actualizada exitosamente'
        ],200);
    }

    public function destroy(Marca $marca)
    {
        $marca->delete();
        return response()->json([
            'status' => true,
            'message' => 'Marca eliminada exitosamente'
        ],200);

    }
}
