<?php

namespace App\Http\Controllers;

use App\Models\Accesorio;
use App\Models\Marca;
use DB;
use Illuminate\Http\Request;

class AccesorioController extends Controller
{
    
    public function index()
    {
        $accesorios = Accesorio::select('accesorios.*',
        'marcas.nombre as marca')
        ->join('marcas','marcas.id','=','accesorios.marca_id')
        ->paginate(10);
        return response()->json($accesorios);
    }

    
    public function store(Request $request)
    {
        $rules = [
            'accesorio' => 'required|string|min:1|max:100',
            'genero' => 'required|string|min:1|max:100',
            'precio' => 'required|string|min:1|max:100',
            'descripcion' => 'required|string|min:1|max:500',
            'marca_id' => 'required|numeric'
        ];
        $validacion = \Validator::make($request->input(),$rules);
        if($validacion->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validacion->errors()->all()
            ],400);
        }
        $accesorio = new Accesorio($request->input());
        $accesorio->save();
        return response()->json([
            'status' => true,
            'message' => 'Accesorio registrado correctamente'
        ],200);
    }

    
    public function show(Accesorio $accesorio)
    {
        return response()->json(['status' => true, 'data' => $accesorio]);
    }

    
    public function update(Request $request, Accesorio $accesorio)
    {
        $rules = [
            'accesorio' => 'required|string|min:1|max:100',
            'genero' => 'required|string|min:1|max:100',
            'precio' => 'required|string|min:1|max:100',
            'descripcion' => 'required|string|min:1|max:500',
            'marca_id' => 'required|numeric'
        ];
        $validacion = \Validator::make($request->input(),$rules);
        if($validacion->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validacion->errors()->all()
            ],400);
        }
        $accesorio->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Accesorio acualizado correctamente'
        ],200);
    }

    public function destroy(Accesorio $accesorio)
    {
        $accesorio->delete();
        return response()->json([
            'status' => true,
            'message' => 'Accesorio eliminado correctamente'
        ],200);
    }

    public function AccesoriosByMarca(){
        $accesorios = Accesorio::select(DB::raw('count(accesorios.id) as count, marcas.nombre'))
        ->rightJoin('marcas','marcas.id','=','accesorios.marca_id')
        ->groupBy('marcas.nombre')
        ->get();

        return response()->json($accesorios);
    }

    public function all()
    {
        $accesorios = Accesorio::select('accesorios.*', 'marcas.nombre as marca')
            ->join('marcas', 'marcas.id', '=', 'accesorios.marca_id')
            ->get();
        return response()->json($accesorios);
    }
}
