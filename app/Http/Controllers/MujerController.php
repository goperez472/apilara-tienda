<?php

namespace App\Http\Controllers;

use App\Models\Mujer;
use App\Models\Marca;
use DB;
use Illuminate\Http\Request;

class MujerController extends Controller
{
    
    public function index()
    {
        $mujers = Mujer::select('mujers.*',
        'marcas.nombre as marca')
        ->join('marcas','marcas.id','=','mujers.marca_id')
        ->paginate(10);
        return response()->json($mujers);
    }

    
    public function store(Request $request)
    {
        $rules = [
            'prenda' => 'required|string|min:1|max:100',
            'talla' => 'required|string|min:1|max:100',
            'precio' => 'required|string|min:1|max:100',
            'descripcion' => 'required|string|min:1|max:500',
            'marca_id' => 'required|numeric'
        ];
        $validacion = \Validator::make($request->input(),$rules);
        if($validacion->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validacion->errors()->all()
            ],400);
        }
        $mujer = new Mujer($request->input());
        $mujer->save();
        return response()->json([
            'status' => true,
            'message' => 'Producto registrado correctamente'
        ],200);
    }

    
    public function show(Mujer $mujer)
    {
       return response()->json(['status' => true, 'data' => $mujer]);
    }

    
    public function update(Request $request, Mujer $mujer)
    {
        $rules = [
            'prenda' => 'required|string|min:1|max:100',
            'talla' => 'required|string|min:1|max:100',
            'precio' => 'required|string|min:1|max:100',
            'descripcion' => 'required|string|min:1|max:500',
            'marca_id' => 'required|numeric'
        ];
        $validacion = \Validator::make($request->input(),$rules);
        if($validacion->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validacion->errors()->all()
            ],400);
        }
        $mujer->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Producto acualizado correctamente'
        ],200);
    }

    public function destroy(Mujer $mujer)
    {
        $mujer->delete();
        return response()->json([
            'status' => true,
            'message' => 'Producto eliminado correctamente'
        ],200);
    }

    public function MujersByMarca()
{
    $mujers = Mujer::select(DB::raw('count(mujers.id) as count, marcas.nombre'))
        ->rightJoin('marcas', 'marcas.id', '=', 'mujers.marca_id')
        ->groupBy('marcas.nombre')
        ->get();

    return response()->json($mujers);
}

    public function all()
{
    $mujeres = Mujer::select('mujers.*', 'marcas.nombre as marca')
        ->join('marcas', 'marcas.id', '=', 'mujers.marca_id')
        ->get();

    return response()->json($mujeres);
}
}
