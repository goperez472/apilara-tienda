<?php

namespace App\Http\Controllers;

use App\Models\Hombre;
use App\Models\Marca;
use DB;
use Illuminate\Http\Request;

class HombreController extends Controller
{
    
    public function index()
    {
        $hombres = Hombre::select('hombres.*',
        'marcas.nombre as marca')
        ->join('marcas','marcas.id','=','hombres.marca_id')
        ->paginate(10);
        return response()->json($hombres);
    }

    
    public function store(Request $request)
    {
        $rules = [
            'prenda' => 'required|string|min:1|max:100',
            'talla' => 'required|string|min:1|max:100',
            'precio' => 'required|string|min:1|max:100',
            'descripcion' => 'required|string|min:1|max:500',
            'marca_id' => 'required|numeric'
        ];
        $validacion = \Validator::make($request->input(),$rules);
        if($validacion->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validacion->errors()->all()
            ],400);
        }
        $hombre = new Hombre($request->input());
        $hombre->save();
        return response()->json([
            'status' => true,
            'message' => 'Producto registrado correctamente'
        ],200);
    }

    
    public function show(Hombre $hombre)
    {
       return response()->json(['status' => true, 'data' => $hombre]);
    }

    
    public function update(Request $request, Hombre $hombre)
    {
        $rules = [
            'prenda' => 'required|string|min:1|max:100',
            'talla' => 'required|string|min:1|max:100',
            'precio' => 'required|string|min:1|max:100',
            'descripcion' => 'required|string|min:1|max:500',
            'marca_id' => 'required|numeric'
        ];
        $validacion = \Validator::make($request->input(),$rules);
        if($validacion->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validacion->errors()->all()
            ],400);
        }
        $hombre->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Producto acualizado correctamente'
        ],200);
    }

    public function destroy(Hombre $hombre)
    {
        $hombre->delete();
        return response()->json([
            'status' => true,
            'message' => 'Producto eliminado correctamente'
        ],200);
    }

    public function HombresByMarca()
{
    $hombres = Hombre::select(DB::raw('count(hombres.id) as count, marcas.nombre'))
        ->rightJoin('marcas', 'marcas.id', '=', 'hombres.marca_id')
        ->groupBy('marcas.nombre')
        ->get();

    return response()->json($hombres);
}

    public function all(){
        $hombres = Hombre::select('hombres.*',
        'marcas.nombre as marca')
        ->join('marcas','marcas.id','=','hombres.marca_id')
        ->get();
        return response()->json($hombres);
    }
}
