<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mujer extends Model
{
    use HasFactory;
    protected $fillable = ['prenda','talla','precio','descripcion','marca_id'];
}
