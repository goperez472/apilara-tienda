<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Accesorio>
 */
class AccesorioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'accesorio' => $this->faker->word,
            'genero' => $this->faker->name,
            'precio' => $this->faker->numberBetween(100, 5000),
            'descripcion' => $this->faker->text,
            'marca_id' => $this->faker->numberBetween(1, 10)
        ];
    }
}
