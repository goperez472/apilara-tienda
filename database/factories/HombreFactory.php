<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Hombre>
 */
class HombreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'prenda' => $this->faker->word,
            'talla' => $this->faker->word,
            'precio' => $this->faker->numberBetween(100, 5000),
            'descripcion' => $this->faker->text,
            'marca_id' => $this->faker->numberBetween(1, 10)
        ];
    }
}
