<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hombres', function (Blueprint $table) {
            $table->id();
            $table->string('prenda',100);
            $table->string('talla',100);
            $table->string('precio',100);
            $table->string('descripcion',500);
            $table->foreignId('marca_id')->constrained('marcas')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hombres');
    }
};
