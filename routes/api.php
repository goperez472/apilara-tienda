<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MarcaController;
use App\Http\Controllers\MujerController;
use App\Http\Controllers\HombreController;
use App\Http\Controllers\AccesorioController;
use App\Http\Controllers\AuthController;

/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('auth/register',[AuthController::class, 'create']);
Route::post('auth/login',[AuthController::class, 'login']);

//poteje las rutas
Route::middleware(['auth:sanctum'])->group(function () {
    // rutas de las tablas
        Route::resource('marcas', MarcaController::class);
        Route::resource('mujers', MujerController::class);
        Route::resource('hombres', HombreController::class);
        Route::resource('accesorios', AccesorioController::class);

        // Prendas de hombre por marca
        Route::get('hombresall', [HombreController::class, 'all']);
        Route::get('hombresbymarca', [HombreController::class, 'HombresByMarca']);

        // Prendas de mujer por marca
        Route::get('mujersall', [MujerController::class, 'all']);
        Route::get('mujersbymarca', [MujerController::class, 'MujersByMarca']);

        // Accesorios por marca
        Route::get('accesoriosall', [AccesorioController::class, 'all']);
        Route::get('accesoriosbymarca', [AccesorioController::class, 'AccesoriosByMarca']);

        //Ruta de cerrar secion
        Route::get('auth/logout',[AuthController::class, 'logout']);
});